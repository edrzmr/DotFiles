#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

GNUPGHOME=$(mktemp -d -t gpg-home-"$(date +%Y-%m-%d)"-XXXXXXXXXX)

die() {
  echo "========================="
  echo "ERROR: $1"
  rm -rf "${GNUPGHOME}"
  exit 69
}

#[[ $(id -u) -ne 0 ]] && die "You must be root"

readonly adminPin=${ADMIN_PIN:-""}
readonly userPin=${USER_PIN:-""}
readonly resetCode=${RESET_CODE:-""}
readonly cardHoldName=${CARD_HOLD_NAME:-"Eder Ruiz"}
readonly email=${EMAIL:-"eder.rzmr@proton.me"}
readonly publicKeyUrl=${PUBLIC_KEY_URL:-"https://gitlab.com/edrzmr.gpg"}
readonly gpgSecretKeyAscFile=${GPG_SECRET_KEY_ASC_FILE:-""}
readonly gpgSubKeysAscFile=${GPG_SUBKEYS_ASC_FILE:-""}

[[ -z $adminPin ]] && die "missing ADMIN_PIN"
[[ -z $userPin ]] && die "missing USER_PIN"
[[ -z $resetCode ]] && die "missing RESET_CODE"
[[ -z $publicKeyUrl ]] && die "missing PUBLIC_KEY_URL"

[[ -z $gpgSecretKeyAscFile ]] && die "missing GPG_SECRET_KEY_ASC_FILE"
[[ ! -f $gpgSecretKeyAscFile ]] && die "${gpgSecretKeyAscFile} not found"
[[ ! -r $gpgSecretKeyAscFile ]] && die "${gpgSecretKeyAscFile} not readable"

[[ -z $gpgSubKeysAscFile ]] && die "missing GPG_SUBKEYS_ASC_FILE"
[[ ! -f $gpgSubKeysAscFile ]] && die "${gpgSubKeysAscFile} not found"
[[ ! -r $gpgSubKeysAscFile ]] && die "${gpgSubKeysAscFile} not readable"

name=$(echo "$cardHoldName" | cut -d" " -f1)
surname=$(echo "$cardHoldName" | cut -d" " -f2-)
language="en"
readonly name surname language

echo "========================="
echo "ADMIN_PIN=$adminPin"
echo "USER_PIN=$userPin"
echo "RESET_CODE=$resetCode"
echo "PUBLIC_KEY_URL=$publicKeyUrl"
echo "CARD_HOLD_NAME=$cardHoldName"
echo "CARD_HOLD_NAME=$name (given)"
echo "CARD_HOLD_NAME=$surname (surname)"
echo "EMAIL=$email"
echo "GPG_SECRET_ASC_FILE=$gpgSecretKeyAscFile"
echo "GPG_SUBKEYS_ASC_FILE=$gpgSubKeysAscFile"
echo "GNUPGHOME=${GNUPGHOME}"
echo "========================="

cat > "${GNUPGHOME}/gpg.conf" << EOF
cert-digest-algo SHA512
charset utf-8
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
fixed-list-mode
keyid-format 0xlong
list-options show-uid-validity
no-comments
no-emit-version
no-symkey-cache
personal-cipher-preferences AES256 AES192 AES
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed
personal-digest-preferences SHA512 SHA384 SHA256
require-cross-certification
s2k-cipher-algo AES256
s2k-digest-algo SHA512
use-agent
verify-options show-uid-validity
with-fingerprint
EOF

cat > "${GNUPGHOME}/scdaemon.conf" << EOF
disable-ccid
EOF


gpg --card-status || die "Device not found"

gpg --quiet --import "${gpgSecretKeyAscFile}" 2> /dev/null || die "Could not import secret key: ${gpgSecretKeyAscFile}"
gpg --quiet --import "${gpgSubKeysAscFile}" 2> /dev/null || die "Could not import secret subkeys: ${gpgSubKeysAscFile}"
gpg --list-secret-keys

set -eu

yes | ykman openpgp reset > /dev/null 2>&1 || die "Could not reset yubikey"

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --change-pin <<EOF
1
123456
$userPin
$userPin
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --change-pin <<EOF
3
12345678
$adminPin
$adminPin
q

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --change-pin <<EOF
4
$adminPin
$resetCode
$resetCode
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --edit-card <<EOF
admin
url
$publicKeyUrl
$adminPin
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --edit-card <<EOF
admin
name
$surname
$name
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --edit-card <<EOF
admin
login
${cardHoldName} <${email}>
$adminPin
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --edit-card <<EOF
admin
login
${cardHoldName} <${email}>
$adminPin
q
EOF

gpg --no-tty --command-fd=0 --pinentry-mode=loopback --edit-card <<EOF
admin
lang
${language}
q
EOF

ykman openpgp access set-retries 5 5 5 -f -a "${ADMIN_PIN}"

echo
echo
echo

gpg --command-fd=0 --edit-key "${email}" <<EOF
key 1
keytocard
1
save
EOF

gpg --command-fd=0 --edit-key "${email}" <<EOF
key 2
keytocard
2
save
EOF

gpg --command-fd=0 --edit-key "${email}" <<EOF
key 3
keytocard
3
save
EOF

gpg --card-status

rm -rf "${GNUPGHOME}"
