{
  description = "Flake for Node 16";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=release-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            permittedInsecurePackages = [
              "nodejs-16.20.2"
            ];
          };
        };
      in
      {
        devShells.default = pkgs.mkShell {

          packages = [
            pkgs.nodejs_16
            pkgs.yarn
          ];

          shellHook = ''
            echo "====================================================================================="
          '';

        };
      }
    );
}

