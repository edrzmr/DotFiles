{
  description = "Darwin system flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs }:
  let
    configuration = { pkgs, ... }: {
      # List packages installed in system profile. To search by name, run:
      # $ nix-env -qaP | grep wget
      environment.systemPackages = with pkgs; [
        coreutils
        binutils
        diffutils
        findutils
        gawk
        indent
        gnumake
        gnused
        ripgrep
        watch
        which
        wget
        gzip
        bzip2
        xz
        less
        gnupg
      ];

      homebrew = {
        enable = true;
        brewPrefix = "/opt/homebrew/bin";
        onActivation = {
          cleanup = "zap";
          autoUpdate = true;
          upgrade = true;
        };
        brews = [
          "git-gui"
          "mas"
          "nmap"
          "k3d"
          "docker-completion"
        ];
        casks = [
          "bitwarden"
          "iterm2"
          "postman"
          "jetbrains-toolbox"
          "slack"
          "firefox@developer-edition"
          "docker"
          "alt-tab"
          "lens"
          "wireshark"
          "discord"

          "font-0xproto-nerd-font"
          "font-3270-nerd-font"
          "font-agave-nerd-font"
          "font-anonymice-nerd-font"
          "font-arimo-nerd-font"
          "font-aurulent-sans-mono-nerd-font"
          "font-bigblue-terminal-nerd-font"
          "font-bitstream-vera-sans-mono-nerd-font"
          "font-blex-mono-nerd-font"
          "font-caskaydia-cove-nerd-font"
          "font-caskaydia-mono-nerd-font"
          "font-code-new-roman-nerd-font"
          "font-comic-shanns-mono-nerd-font"
          "font-commit-mono-nerd-font"
          "font-cousine-nerd-font"
          "font-d2coding-nerd-font"
          "font-daddy-time-mono-nerd-font"
          "font-dejavu-sans-mono-nerd-font"
          "font-droid-sans-mono-nerd-font"
          "font-envy-code-r-nerd-font"
          "font-fantasque-sans-mono-nerd-font"
          "font-fira-code-nerd-font"
          "font-fira-mono-nerd-font"
          "font-geist-mono-nerd-font"
          "font-go-mono-nerd-font"
          "font-gohufont-nerd-font"
          "font-hack-nerd-font"
          "font-hasklug-nerd-font"
          "font-heavy-data-nerd-font"
          "font-hurmit-nerd-font"
          "font-im-writing-nerd-font"
          "font-inconsolata-go-nerd-font"
          "font-inconsolata-lgc-nerd-font"
          "font-inconsolata-nerd-font"
          "font-intone-mono-nerd-font"
          "font-iosevka-nerd-font"
          "font-iosevka-term-nerd-font"
          "font-iosevka-term-slab-nerd-font"
          "font-jetbrains-mono-nerd-font"
          "font-lekton-nerd-font"
          "font-liberation-nerd-font"
          "font-lilex-nerd-font"
          "font-m+-nerd-font"
          "font-martian-mono-nerd-font"
          "font-meslo-lg-nerd-font"
          "font-monaspace-nerd-font"
          "font-monocraft-nerd-font"
          "font-monofur-nerd-font"
          "font-monoid-nerd-font"
          "font-mononoki-nerd-font"
          "font-noto-nerd-font"
          "font-open-dyslexic-nerd-font"
          "font-overpass-nerd-font"
          "font-profont-nerd-font"
          "font-proggy-clean-tt-nerd-font"
          "font-recursive-mono-nerd-font"
          "font-roboto-mono-nerd-font"
          "font-sauce-code-pro-nerd-font"
          "font-shure-tech-mono-nerd-font"
          "font-space-mono-nerd-font"
          "font-symbols-only-nerd-font"
          "font-terminess-ttf-nerd-font"
          "font-tinos-nerd-font"
          "font-ubuntu-mono-nerd-font"
          "font-ubuntu-nerd-font"
          "font-ubuntu-sans-nerd-font"
          "font-victor-mono-nerd-font"
          "font-zed-mono-nerd-font"
        ];
      };

      # Auto upgrade nix package and the daemon service.
      # services.nix-daemon.enable = true;
      # nix.package = pkgs.nix;
      nix.useDaemon = true;

      # Necessary for using flakes on this system.
      nix.settings.experimental-features = "nix-command flakes";

      # Create /etc/zshrc that loads the nix-darwin environment.
      programs.zsh.enable = true;  # default shell on catalina

      # Set Git commit hash for darwin-version.
      system.configurationRevision = self.rev or self.dirtyRev or null;

      # Used for backwards compatibility, please read the changelog before changing.
      # $ darwin-rebuild changelog
      system.stateVersion = 4;

      # The platform the configuration will be used on.
      nixpkgs.hostPlatform = "aarch64-darwin";
    };
  in
  {
    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#AM-EMARIA-M2
    darwinConfigurations."AM-EMARIA-M2" = nix-darwin.lib.darwinSystem {
      modules = [ configuration ];
    };

    # Expose the package set, including overlays, for convenience.
    darwinPackages = self.darwinConfigurations."AM-EMARIA-M2".pkgs;
  };
}
