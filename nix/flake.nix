{
  description = "Sweet Home Alabama";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    homeManager.url = "github:nix-community/home-manager";
    homeManager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, homeManager, ...}: {

    homeConfigs.x86_64-linux.zeph.edrzmr = homeManager.lib.homeManagerConfiguration {
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };
      modules = [ ./home-manager/x86_64.linux.zeph.edrzmr.nix ];
    };

    homeConfigs.x86_64-linux.vl-aus-domdv608.edrzmr = homeManager.lib.homeManagerConfiguration {
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };
      modules = [ ./home-manager/x86_64.linux.vl-aus-domdv608.edrzmr.nix ];
    };

    homeConfigs.aarch64-darwin.AM-EMARIA-M2.emaria = homeManager.lib.homeManagerConfiguration {
      pkgs = import nixpkgs {
        system = "aarch64-darwin";
        config.allowUnfree = true;
        config.allowBroken = true;
      };
      modules = [ ./home-manager/aarch64.darwin.AM-EMARIA-M2.emaria.nix ];
    };

  };
}
