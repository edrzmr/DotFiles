#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

installer_url="https://install.determinate.systems/nix"
installer_path="/tmp/nix-installer-downloader"
readonly installer_url installer_path

install_nix_darwin() {
  [[ ! -L /etc/nix/nix.conf && -f /etc/nix/nix.conf ]] && \
    sudo mv /etc/nix/nix.conf /etc/nix/nix.before-nix-darwin

  nix run nix-darwin \
    --extra-experimental-features nix-command \
    --extra-experimental-features flakes \
    -- \
      switch \
        --flake ../nix/flakes/nix-darwin
}

curl \
  --proto '=https' \
  --tlsv1.2 \
  -sSf \
  -L "$installer_url" \
  --output "$installer_path"
  
sh $installer_path install

source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh

case $(nix-instantiate --eval --expr builtins.currentSystem | tr -d "\"" | cut -d- -f2) in
  darwin)
    install_nix_darwin
    ;;
  *)
    ;;
esac
