{ config, pkgs, ... }: {

  home.username = "emaria";
  home.homeDirectory = "/Users/emaria";
  home.stateVersion = "24.05";

  home.sessionVariables = {
    _LOCAL_BREW_DIR = "/opt/homebrew";
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    btop
    htop
    tig
    tree
    pstree
    neofetch
    jq
    qpdf
    m-cli
    yq
    gnused
    unixtools.netstat

    kubectl
    kubernetes-helm
    starship
  ];

  home.file."Library/KeyBindings/DefaultKeyBinding.dict".source = ./files/darwin-keybinding/DefaultKeyBinding.dict;
  programs.zsh = import ./modules/zsh.nix pkgs;
  programs.direnv = import ./modules/direnv.nix pkgs;

  home.file.".zsh/completions/.placeholder".text = "";
}
