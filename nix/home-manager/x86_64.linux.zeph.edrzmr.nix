{ config, pkgs, ... }: {

  home.username = "edrzmr";
  home.homeDirectory = "/home/edrzmr";
  home.stateVersion = "24.05";

  home.sessionVariables = {
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    kubectl
    google-cloud-sdk
    kubernetes-helm
    unrar

    paperkey

    ventoy-bin-full

    nmap
    htop
    btop
    starship
  ];

  programs.zsh = import ./modules/zsh.nix pkgs;
  programs.direnv = import ./modules/direnv.nix pkgs;

  # envrc.d
  home.file.".config/envrc.d/scala-k8s.envrc".source = ./files/envrc.d/scala-k8s.envrc;

  home.file.".zsh/completions/.placeholder".text = "";

  #########
  # dconf #
  #########
  dconf.settings = {
    "com/gexperts/Tilix" = {
      control-scroll-zoom = true;
      enable-wide-handle = true;
      middle-click-close = false;
      prompt-on-new-session = false;
      quake-specific-monitor = 0;
      session-name = "\${title}";
      sidebar-on-right = true;
      tab-position = "top";
      terminal-title-style = "small";
      theme-variant = "dark";
      use-tabs = true;
      window-style= "disable-csd-hide-toolbar";
    };

    "com/gexperts/Tilix/profiles" = {
      default = "Dark";
      list = [
        "SemiDark"
        "Dark"
      ];
    };

    "com/gexperts/Tilix/profiles/SemiDark" = {
      background-color = "#303030";
      badge-color = "#AC7EA8";
      badge-color-set = true;
      bold-color-set = false;
      cursor-colors-set = false;
      foreground-color = "#EFEFEF";
      highlight-colors-set = false;
      login-shell = true;
      palette = [
        "#000000"
        "#CC0000"
        "#4D9A05"
        "#C3A000"
        "#3464A3"
        "#754F7B"
        "#05979A"
        "#D3D6CF"
        "#545652"
        "#EF2828"
        "#89E234"
        "#FBE84F"
        "#729ECF"
        "#AC7EA8"
        "#34E2E2"
        "#EDEDEB"
      ];
      terminal-title = "\${id}: \${title}";
      use-theme-colors = false;
      visible-name = "Uhet";
    };

    "com/gexperts/Tilix/profiles/Dark" = {
      allow-bold = true;
      background-color = "#300A24";
      bold-color = "#ffffff";
      bold-color-set = false;
      bold-is-bright = true;
      cell-height-scale = 1.0;
      cell-width-scale = 1.0;
      cjk-utf8-ambiguous-width = "narrow";
      cursor-background-color = "#000000";
      cursor-blink-mode = "system";
      cursor-colors-set = false;
      cursor-foreground-color = "#ffffff";
      cursor-shape = "block";
      custom-command = "";
      default-size-columns = 160;
      default-size-rows = 50;
      delete-binding = "delete-sequence";
      dim-transparency-percent = 0;
      draw-margin = 256;
      encoding = "UTF-8";
      exit-action = "close";
      font = "Monospace 12";
      foreground-color = "#FFFFFF";
      highlight-background-color = "#000000";
      highlight-colors-set = false;
      highlight-foreground-color = "#ffffff";
      login-shell = true;
      notify-silence-enabled = false;
      notify-silence-threshold = 0;
      palette = [
        "#000000"
        "#CC0000"
        "#4D9A05"
        "#C3A000"
        "#3464A3"
        "#754F7B"
        "#05979A"
        "#D3D6CF"
        "#545652"
        "#EF2828"
        "#89E234"
        "#FBE84F"
        "#729ECF"
        "#AC7EA8"
        "#34E2E2"
        "#EDEDEB"
      ];
      rewrap-on-resize = true;
      scroll-on-keystroke = true;
      scroll-on-output = false;
      scrollback-lines = 8192;
      scrollback-unlimited = false;
      select-by-word-chars = "-,./?%&#:_";
      shortcut = "disabled";
      show-scrollbar = true;
      terminal-bell = "sound";
      terminal-title = "\${id}: \${title}";
      text-blink-mode = "always";
      use-custom-command = false;
      use-system-font = true;
      use-theme-colors = true;
      visible-name = "Dark";
    };
  };
}
