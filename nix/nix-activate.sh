#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

platform=$(nix-instantiate --eval --expr 'builtins.currentSystem' | tr -d "\"")
kernel=$(echo "$platform" | cut -d- -f2)
hostname=$(hostname)
username=$(whoami)
readonly platform kernel hostname username

nix build .#homeConfigs."${platform}"."${hostname}"."${username}".activationPackage --show-trace
./result/activate

[[ $kernel != darwin ]] && exit 0

/run/current-system/sw/bin/darwin-rebuild switch --flake ./flakes/nix-darwin
